//
//  main.c
//  C-Tutorium
//
//  Created by Jan Galler on 19/10/14.
//  Copyright (c) 2014 Jan Galler. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

/** AUFGABE 1 **/

void aufgabe_1()
{
    // deklarieren der benötigten variablen
    int counter;
    int untere_grenze;
    int obere_grenze;
    
    // untere grenze abfragen
    printf("Untere Grenze: ");
    // untere grenze einlesen
    scanf("%d",&untere_grenze);
    
    // obere grenze abfragen
    printf("Obere Grenze: ");
    // obere grenze einlesen
    scanf("%d",&obere_grenze);
    
    // schleifen-variable auf untere_grenze setzen
    counter = untere_grenze;
    
    // so lange die schleifen-variable kleiner ist als die obere grenze ausführen:
    while (counter < obere_grenze)
    {
        // wenn der modulu schleifen-variable%2 nicht 0 ist, ist die schleifen-variable ungerade
        if((counter % 2) != 0)
        {
            // schleifen-variable ist ungerade, also ausgeben
            printf("%d\n", counter);
        }
        
        // bei jedem schleifen-durchlauf die schleifen-variable erhöhen
        counter++;
    }

}


/** AUFGABE 2 **/

int string_laenge(char string[])
{
    // deklariere zähler-variable
    int counter;
    
    // schleife so lange ausführen, wie chars im char-array/string sind
    while (string[ counter ])
    {
        // zähler erhöhen
        counter++;
    }
    
    // nach dem zählen, zähler-variable zurückgeben
    return counter;
}

void aufgabe_2()
{
    int buffer = 1024;
    char eingabe[buffer];
    char check[] = "Ende";
    
    printf("Gebe Text ein, um die Länge ermitteln zu lassen, zum Beenden \"Ende\" einggeben. Jeweils bestätigen mit Enter:\n");

    // so lange ausführen, wie eingabe ungleich "Ende" ist
    while (strcmp(eingabe,check) != 0)
    {
        // text von der konsole einlesen
        scanf("%s",eingabe);
        
        // länge ermitteln lassen und ausgeben
        printf("String Länge: %d\n", string_laenge(eingabe));
    }
}


/** MAIN **/

int main(int argc, const char * argv[])
{
    // aufgabe 1 ausführen
    //aufgabe_1();
    
    // aufgabe 2 ausführen
    aufgabe_2();
    
    
    // programm erfolgreich beenden
    return 0;
}
